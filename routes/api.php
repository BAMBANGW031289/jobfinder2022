<?php

use App\Http\Controllers\JobController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('/{language}', [LanguageController::class, 'switch'])->name('localization.switch');


//job routes
Route::middleware('api')->group(function () {
    Route::get('search', [JobController::class, 'search'])->name('job.search');

    //pages api
    Route::get('company-categories', [JobController::class, 'getCategories'])->name('job.getCategories');
    Route::get('job-titles', [JobController::class, 'getAllByTitle'])->name('job.getAllByTitle');
    Route::get('companies', [JobController::class, 'getAllOrganization'])->name('job.getAllOrganization');

    //home jobfinder
    Route::get('companies_jobcount/{id}', [JobController::class, 'getAllOrganization_Count'])->name('job.getAllOrganization_Count');
    Route::get('search_self', [JobController::class, 'SearchJobFinder'])->name('jobfinder.search.self');
    Route::get('search_company', [JobController::class, 'getCategories_company'])->name('jobfinder.search.company');
    Route::get('search_organization', [JobController::class, 'getAllOrganization_company'])->name('jobfinder.search.organization');
    Route::get('get_company/{id}', [JobController::class, 'getBy_company'])->name('jobfinder.getBy.organization');
    Route::get('get_perjob', [JobController::class, 'getBy_PerJob'])->name('jobfinder.getBy.PerJob');
});

