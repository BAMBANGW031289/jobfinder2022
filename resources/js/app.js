require("./bootstrap");

window.Vue = require("vue").default;

import Vue from "vue";
// require("./progressbar");

import VueRouter from "vue-router";
Vue.use(VueRouter);

import routes from "./routes";
const router = new VueRouter({
    mode: "history",
    //base: process.env.BASE_URL,
    routes
    // mode: "hash"

});

import VueCarousel from '@chenfengyuan/vue-carousel';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)


Vue.component(VueCarousel.name, VueCarousel);

// Vue.component('language-switcher', require('./components/LanguageSwitcher.vue').default);


Vue.component(
    "sidebar-component",
    require("./components/Sidebar.vue").default
);

Vue.component(
    "app-component",
    require("./components/AppComponent.vue").default
);
Vue.component(
    "app-post-job",
    require("./components/JobFinderContent.vue").default
);
Vue.component(
    "search-job",
    require("./components/SearchResultUser.vue").default
);
Vue.component("pagination", require("laravel-vue-pagination"));

Vue.component(
    "jobFinder-header",
    require('./components/includesJobFinder/Header.vue').default
);

Vue.component(
    "jobFinder-header2",
    require('./components/includesJobFinder/Header2.vue').default
);

Vue.component(
    "jobFinder-footer",
    require('./components/includesJobFinder/Footer.vue').default
);
const app = new Vue({
    el: "#app",
    router
});
