import JobComponent from "./components/JobComponent";
import Organization from "./pages/Organization";
import JobCategory from "./pages/JobCategory";
import JobTitle from "./pages/JobTitle";
import JobCompany from "./pages/JobCompany";
import SearchResultUser from "./components/SearchResultUser";
import searchResult from "./components/SearchResult";
import Home from "./components/JobFinderHome";
import Content from "./components/JobFinderContent";
import Errors from "./components/ErrorComponent";



const routes = [
    {
        path: "/",
        component: Home
    },
    {
        path: "*",
        component: Errors
    },
    {
       // path: "/searchResult/:key/:key2/:key3",
        path: "/searchResult",
        name: "searchResult",
        component: searchResult,
        meta: {
            title: "Pintar Kerja - Jobfinder" 
        }
    },
    {
        path: "/job-company/:key",
        name: "JobCompany",
        component: JobCompany,
        meta: {
            title: "Pintar Kerja - Jobfinder" 
        }
    },
    {
        path: "/post",
        component: Content
    },
    {
        path: "/job-search",
        component: SearchResultUser
    },
    {
        path: "/jobs-by-organization",
        component: Organization
    },
    {
        path: "/jobs-by-title",
        component: JobTitle
    },
    {
        path: "/jobs-by-category",
        component: JobCategory
    }
];
export default routes;
