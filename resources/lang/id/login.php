<?php

return [

    'header' => [
           'welcome'         => 'Selamat Datang di Jobfinder',
           'welcome_content' => 'login dengan email & password anda.',
    ],

    'form' => [
        'input_remember'     => 'Ingat Saya',
        'forgot_password'    => 'Lupa Password',
        'or'                 => 'atau',
        'login_google'       => 'Login dengan Google',
        'create_account'     => 'Buat Akun Baru ?',
        'signup_now'         => 'Daftar Sekarang',
    ],

    'login_poster' => [
        'slogon'             => 'Dapatkan Pekerjaan',
        'slogon_2'           => 'Yang Anda Impikan',
        'paragraf'           => 'Kami menyediakan ribuan lowongan kerja untuk anda,
                                 dan juga ribuan kandidat terbaik bagi perusahaan anda'
    ],

    'nav_lang' => [
        'select_indo'        => 'Indonesia',
        'select_eng'         => 'Inggris'
    ],
];