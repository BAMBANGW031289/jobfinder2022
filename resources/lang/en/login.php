<?php

return [

    'header' => [
           'welcome'         => 'Welcome to Jobfinder',
           'welcome_content' => 'login with your email & password.',
    ],

    'form' => [
        'input_remember'     => 'Remember Me',
        'forgot_password'    => 'Forget the Password',
        'or'                 => 'or',
        'login_google'       => 'Login With Google',
        'create_account'     => 'Create Account ?',
        'signup_now'         => 'Sign up FREE Now',
    ],

    'login_poster' => [
        'slogon'             => 'Get the Job',
        'slogon_2'           => 'You Dreamed of',
        'paragraf'           => 'We provide thousands of job vacancies for you,
                                 and also thousands of the best candidates for your company'
    ],

    'nav_lang' => [
        'select_indo'        => 'Indonesian',
        'select_eng'         => 'English'
    ],
];
