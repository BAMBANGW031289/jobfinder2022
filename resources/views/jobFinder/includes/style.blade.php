@stack('before-style')
<!-- Google Font -->
<link href="https://fonts.googleapis.com/css2?family=Heebo&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@500&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@500&display=swap" rel="stylesheet">
<link rel="shortcut icon" href="{{ asset('assets/img/icon.png') }}" type="image/x-icon" >
<!-- Css Styles -->
{{--<link rel="stylesheet" href="{{ mix('css/guestuserpage.css') }}">--}}
<link rel="stylesheet" href="{{ asset('assets/css/style-first.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets/css/themify-icons.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets/css/elegant-icons.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets/css/nice-select.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.min.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets/css/slicknav.min.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ mix('css/app.css') }}">

@stack('after-style')
{{-- @include('jobFinder.includes.style') --}}
