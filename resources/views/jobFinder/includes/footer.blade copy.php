<footer class="footer-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="footer-left">
                    <img class="ml-2" src="{{ asset('assets/img/logo.png') }}" alt="" width="110"/>
                    <div class="mt-4">
                        <img src="{{ asset('assets/img/googleplay.png') }}" alt="" />
                    </div>
                    
                </div>
            </div>
            <div class="col-lg-3 ">
                <div class="footer-widget">
                    <h5>Produk</h5>
                    <ul>
                        <li><a href="#">Tentang Pintar Kerja</a></li>
                        <li><a href="#">Bermitra dengan Pintar Kerja</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="footer-widget">
                    <h5>Support</h5>
                    <ul>
                        <li><a href="#">Pusat Bantuan</a></li>
                        <li><a href="#">Hubungi Kami</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 ">
                <div class="footer-left">
                    <h5>Ikuti Kami</h5>
                    <div class="footer-social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-reserved">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright-text">
                        &copy;
                        2021
                        
                        Pintar Kerja All Rights Reserved
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>