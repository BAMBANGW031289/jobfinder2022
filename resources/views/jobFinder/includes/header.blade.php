<header class=" skin-red header-section border-bottom">
    <nav class="navbar">
        <div class="navbar-brand ">
            <div class="row">
                <div class="border-right ml-3">
                    <router-link to='/'>
                        <img src="{{ asset('assets/img/logo.png') }}" alt="" class="px-1" width="80">
                    </router-link>
                </div>
                <div>
                    <img src="{{ asset('assets/img/prakerja.png') }}" alt="">
                </div>
            </div>
        </div>
        <div class="ht-right" id="">
        <div class="top-social">
        <router-link to="home" class="nav-link" href="#">Prakerja <span class="sr-only">(current)</span></router-link>
        <a class="nav-link" href="#">Bantuan</a>
        @php
            $user = Auth::user();
            if ($user) {
                 echo '<router-link to="my-course" class="nav-link btn btn-main" href="#">Kursus Saya</router-link>';
            } else {
                echo '<router-link to="register" class="nav-link btn btn-grey" href="#">Daftar</router-link>';
                echo '<router-link to="login" class="nav-link btn btn-main" href="#">Masuk</router-link>';
            }
        @endphp
        </div>
    </div>
        </nav>
    </div>
</header>
