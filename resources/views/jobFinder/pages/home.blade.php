<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8" />
    <meta name="description" content="Pintar kerja Template" />
    <meta name="keywords" content="pintar kerja, prakerja, belajar, latihan, html, css, bahasa" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Pintar Kerja - jobFinder</title>
    @include('jobFinder.includes.style')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body>
    <div id="app">
        <!-- content -->
        <router-view></router-view>
        <!-- content End -->
    </div>
    @include('jobFinder.includes.script')
</body>
</html>
