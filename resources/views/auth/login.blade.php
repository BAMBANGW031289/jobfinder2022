@extends('layouts.auth')

@section('content')
<div class="container my-4">
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-6 px-0">
            <div class="login-container" style="background-color: wheat">
                <div class="login-header mb-3">
                    <h3> <img src="{{asset('images/logo/joblister.png')}}" width="50px;" alt="">JobsFinder Login</h3>
                    <p class="login-header-title">@lang('login.header.welcome')</p>
                    <p class="text-muted">@lang('login.header.welcome_content')</p>
                </div>
                <div class="login-form">
                    <form action="{{route('login')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                  <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                                </div>
                            <input id="email" type="email" placeholder="E-mail address" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                  <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
                                </div>
                            <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            </div>
                        </div>
                        <div class="form-group mb-0">
                            <input type="checkbox" id="rememberMe" name="remember" {{old('remember')?'checked':''}}>
                            <label for="rememberMe">@lang('login.form.input_remember')</label>
                        </div>
                        <div class="form-group">
                            <a href="#" class="secondary-link">@lang('login.form.forgot_password')</a>
                        </div>
                        <button type="submit" class="btn btn-info btn-block">Login</button>
                        <div class="or-container">
                            <div class="line-separator"></div>
                            <div class="or-label">@lang('login.form.or')</div>
                            <div class="line-separator"></div>
                        </div>
                    <a href="{{ '/auth/redirect'}}" class="btn btn-light btn-block"><img src="https://img.icons8.com/color/18/000000/google-logo.png"/>&nbsp;@lang('login.form.login_google')</a>
                    </form>
                    <div class="my-3">
                        <p>@lang('login.form.create_account')&nbsp;<a href="/register">@lang('login.form.signup_now')</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 px-0">
            <div class="login-poster">
                {{-- <img src="" alt=""> --}}
                <h2 class="mb-3 slogon">@lang('login.login_poster.slogon')<br>@lang('login.login_poster.slogon_2')</h2>
                <p class="text-white lead">@lang('login.login_poster.paragraf')</p>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
<style>
.login-poster {
   background-image: url('{{asset("images/login-bg.jpg")}}');
    background-image: linear-gradient(
            to bottom,
            rgba(0, 0, 0, 0.5),
            rgba(0, 0, 0, 0.35)
        ),
        url('{{asset("images/login-bg.jpg")}}');
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
}
.or-container {
    align-items: center;
    color: #666;
    display: flex;
    margin: 25px 0
}

.line-separator {
    background-color: #666;
    flex-grow: 5;
    height: 1px
}

.or-label {
    flex-grow: 1;
    margin: 0 15px;
    text-align: center
}
</style>
@endpush