@extends('layouts.app')

@section('layout-holder')
    {{-- styles here are placed account-layout.scss   --}}
    @include('inc.navbar')
    <div class="container my-4">
        <div class="jumbotron jumbotron-fluid imj">
            <div class="container" >
              <h1>Ingin Mendapat Pekerjaan Yang Anda Impikan ?</h1>
              <p>Puluhan Ribu Perusahaan Menanti Anda...</p>
            </div>
          </div>
     {{-- <div class="row">   
      <img src="{{url('assets/img/jobFinder/headerJobFinder.png')}}">
       </div> --}}
        <div class="account-layout">
            <div class="account-hdr border">
                <h5><i class="fas fa-cog"></i> Setting Account</h5>
            </div>
            <div class="account-bdy">
                <div class="row">
                    <div class="col-sm-12 col-md-3 pr-md-0 pr-sm-3">
                        {{-- account-navigation from inc --}}
                        @include('inc.account-nav')
                    </div>
                    <div class="col-sm-12 col-md-9 pl-md-0 pl-sm-3 ">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('inc.footer')
@endsection
@push('css')
<style>
.imj{   
background-image:url("/assets/img/jobFinder/headerJobFinder.png");
background-repeat: no-repeat;
background-attachment: fixed;
color:white !important;
height:320px;
}
</style>
@endpush