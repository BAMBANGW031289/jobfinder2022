@push('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.5.0/css/flag-icon.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
@endpush

<nav class="navbar navbar-expand-md navbar-white bg-white border-bottom sticky-top" id="navbar">
  <div class="container">
    @role('')
    <a href="{{URL('/search')}}" class="navbar-brand">
        JobsFinder
      </a>
  @endrole  
  @role('admin')
  <a href="{{URL('/post')}}" class="navbar-brand">
      JobsFinder
    </a>
    @endrole
    @role('authors')
    <a href="{{URL('/search')}}" class="navbar-brand">
        JobsFinder
      </a>
      @endrole
      @role('user')
      <a class="navbar-brand" href="/post"><img :src="'../assets/img/jobFinder/jobFinder.png'" alt="" /></a>
      <ul class="navbar-nav mr-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Pelatihan</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#" tabindex="-1" aria-disabled="true">Hire</a>
        </li>
      </ul>
        @endrole
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
     <i class="fas fa-bars"></i>
    </button>
    @guest
    <a class="navbar-brand" href="/"><img :src="'../assets/img/jobFinder/jobFinder.png'" alt="" /></a>
    @endguest

    @auth
    <a class="navbar-brand" href="/post"><img :src="'../assets/img/jobFinder/jobFinder.png'" alt="" /></a>
      <ul class="navbar-nav mr-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Pelatihan</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#" tabindex="-1" aria-disabled="true">Hire</a>
        </li>
      </ul>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown dropdown-left"> 
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
          <span class="mr-2 d-none d-lg-inline text-gray-600 small" style="font-weight: bold;">{{auth()->user()->name}}</span>   
              @if(Auth::user()->image)
              <img src="{{asset('/storage/images/'.Auth::user()->image)}}" class="img-profile rounded-circle" width="40px" alt="User-Profile-Image">    
              @else
              <img src="{{asset('images/user-profile.png')}}" class="img-profile rounded-circle" alt="User-Profile-Image">
              @endif
          </a>
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown"> 
            @role('admin')
            <a class="dropdown-item" href="{{route('account.dashboard')}}"> <i class="fas fa-cogs fa-sm "></i> Dashboard</a> 
            @endrole
            @role('author')
            <a class="dropdown-item" href="{{route('account.authorSection')}}"> <i class="fa fa-cogs fa-sm "></i> Author Dashboard </a> 
            @endrole
            <a class="dropdown-item" href="{{route('account.index')}}"> <i class="fas fa-user fa-sm "></i> Profile </a> 
            <a class="dropdown-item" href="{{route('account.changePassword')}}"> <i class="fas fa-key fa-sm "></i> Change Password </a> 
              <div class="dropdown-divider"></div> 
              <a class="dropdown-item" href="{{route('account.logout')}}"> 
                <i class="fas fa-sign-out-alt"></i> 
                Logout 
              </a>
          </div>
        </li>
        @endauth
        @guest
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01"> 
          <ul class="navbar-nav mr-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="#">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Pelatihan</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#" tabindex="-1" aria-disabled="true">Hire</a>
            </li>
          </ul>
          @if (request()->segment(1) != 'login')
          <div class="navbar-nav ml-2">
           <a class="btn btn-primary inter font-size14" href="/login">Daftar / Masuk</a>
          </div>
          @endif
        &nbsp;
        <div class="dropdown">
          <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
            {{ strtoupper(session('locale') ?? config('app.fallback_locale')) }}
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="{{ url('/login/id')}}"><i class="flag-icon flag-icon-id"></i> @lang('login.nav_lang.select_indo') (ID)</a> 
            <div class="dropdown-divider"></div> 
           <a class="dropdown-item" href="{{ url('/login/en')}}"> 
             <i class="flag-icon flag-icon-gb"></i> 
             @lang('login.nav_lang.select_eng') (EN)
           </a>
          </div>
        </div>
      </div>
       
        {{-- <li class="nav-item dropdown dropdown-left" style="background-color:wheat"> 
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
          <span class="mr-2 d-none d-lg-inline text-gray-600 small" style="font-weight: bold;"> 
            {{ strtoupper(session('locale') ?? config('app.locale')) }} --}}
            {{-- @switch(app()->getLocale())
              @case('id')
           <i class="flag-icon flag-icon-id"></i> --}}
            {{-- <img class="img-profile rounded-circle" src="{{asset('images/user-profile.png')}}" width="40px">  --}}
             {{-- @case('en')
             <i class="flag-icon flag-icon-gb"></i>
             @break
             @default
             @endswitch --}}
          {{-- </span>
          </a>
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown"> 
        
            <a class="dropdown-item" href="{{ url('/login/id')}}"><i class="flag-icon flag-icon-id"></i> @lang('login.nav_lang.select_indo') (ID)</a> 
               <div class="dropdown-divider"></div> 
              <a class="dropdown-item" href="{{ url('/login/en')}}"> 
                <i class="flag-icon flag-icon-gb"></i> 
                @lang('login.nav_lang.select_eng') (EN)
              </a>
          </div>
        </li> --}}
        {{-- <li style="background-color:beige" class="nav-item dropdown dropdown-left"> 
          {{-- <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">  --}}
          {{-- <span style="background-color:beige" class="mr-2 d-none d-lg-inline text-gray-600 small" style="font-weight: bold;">{{ app()->getLocale() }} dasdadasdasdas</span> 
            <img class="img-profile rounded-circle" src="" width="40px"> 
          </a>
           <a class="nav-link dropdown-toggle" href="#">
            @switch(app()->getLocale())
            @case('id')   
            <i class="flag-icon flag-icon-id"></i>
            @break('en')
            <i class="flag-icon flag-icon-gb"></i>
            @break
            @default
            @endswitch
          </a>
        </li>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="language"> --}}
          {{-- <a class="nav-link dropdown" href="#">
            @switch(app()->getLocale())
            @case('id')   
            <i class="flag-icon flag-icon-id"></i>
            @break('en')
            <i class="flag-icon flag-icon-gb"></i>
            @break
            @default
            @endswitch
          </a>
        --}}
        {{-- <select onchange="changeLang(this)" class="form-control" data-width="fit">
          <option value="id" @if (App::isLocale('id')) selected @endif data-content='<i class="fas fa-flag-usa"></i> English'>Indonesia</option>
        <option value="en" @if (App::isLocale('en')) selected @endif data-content='<span class="flag-icon flag-icon-mx"></span> Indonesia'>English</option>
      </select> --}}
        @endguest
      </ul>
    </li>
    </div>
  </div>
</nav>

{{-- <script>
  const BASE_URL = '{{ url('/') }}';
  const changeLang = (e) => {
      window.location.href = BASE_URL + '/' + e.value;

  }
</script> --}}
