@extends('layouts.job')
{{-- @extends('jobFinder.pages.home') --}}
@section('content')
@include('jobFinder.includes.style')
<section class="job-section">
  {{-- <app-post-job/> --}}
  <search-job/>
</section>
@include('jobFinder.includes.script')  
@endsection
