/*
 Navicat Premium Data Transfer

 Source Server         : lokalan
 Source Server Type    : MySQL
 Source Server Version : 100422
 Source Host           : localhost:3306
 Source Schema         : job_lister

 Target Server Type    : MySQL
 Target Server Version : 100422
 File Encoding         : 65001

 Date: 04/02/2022 06:15:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for companies
-- ----------------------------
DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `company_category_id` int(10) UNSIGNED NOT NULL,
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of companies
-- ----------------------------
INSERT INTO `companies` VALUES (1, 2, 1, 'images/logo/7.png', 'Gabrato company', 'This company Pvt Ltd is the company specialized to help organizations with financial technology solutions. We provide solutions such comprehensive mobile and online payment solutions and gateway facilitating services. We facilitate in online transaction settlement service to merchants and their banks to be able to accept/acquire payments from third party payment sources. We provide technology and solutions for acquiring payment from 3rd party wallets, smart wallets solutions, merchant management solutions and host of other solutions..', 'https://www.companywebsite.com', 'nocover', '2022-01-24 09:18:09', '2022-01-24 09:18:09');

-- ----------------------------
-- Table structure for company_categories
-- ----------------------------
DROP TABLE IF EXISTS `company_categories`;
CREATE TABLE `company_categories`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of company_categories
-- ----------------------------
INSERT INTO `company_categories` VALUES (1, 'IT & Telecommunication');
INSERT INTO `company_categories` VALUES (2, 'Marketing / Advertising');
INSERT INTO `company_categories` VALUES (3, 'General Mgmt');
INSERT INTO `company_categories` VALUES (4, 'Banking / Insurance /Financial Services');
INSERT INTO `company_categories` VALUES (5, 'Construction / Engineering / Architects ');
INSERT INTO `company_categories` VALUES (6, 'Creative / Graphics / Designing');
INSERT INTO `company_categories` VALUES (7, 'Social work');
INSERT INTO `company_categories` VALUES (8, 'hospitality');
INSERT INTO `company_categories` VALUES (9, 'journalism-editor-media');
INSERT INTO `company_categories` VALUES (10, 'Agriculture + Livestock');
INSERT INTO `company_categories` VALUES (11, 'Teaching profession');
INSERT INTO `company_categories` VALUES (12, 'Engineer');
INSERT INTO `company_categories` VALUES (13, 'Sales');
INSERT INTO `company_categories` VALUES (14, 'Leadership');
INSERT INTO `company_categories` VALUES (15, 'Web development');
INSERT INTO `company_categories` VALUES (16, 'Mobile App');
INSERT INTO `company_categories` VALUES (17, 'Sales');
INSERT INTO `company_categories` VALUES (18, 'E-Commerce');
INSERT INTO `company_categories` VALUES (19, 'Others');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for job_applications
-- ----------------------------
DROP TABLE IF EXISTS `job_applications`;
CREATE TABLE `job_applications`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (5, '2020_10_09_104919_create_permission_tables', 1);
INSERT INTO `migrations` VALUES (6, '2020_10_09_144234_create_company_categories_table', 1);
INSERT INTO `migrations` VALUES (7, '2020_10_09_145555_create_companies_table', 1);
INSERT INTO `migrations` VALUES (8, '2020_10_11_024354_create_posts_table', 1);
INSERT INTO `migrations` VALUES (9, '2020_10_12_133736_create_post_user_table', 1);
INSERT INTO `migrations` VALUES (10, '2020_10_13_111952_create_job_applications_table', 1);

-- ----------------------------
-- Table structure for model_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE `model_has_permissions`  (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `model_id`, `model_type`) USING BTREE,
  INDEX `model_has_permissions_model_id_model_type_index`(`model_id`, `model_type`) USING BTREE,
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for model_has_roles
-- ----------------------------
DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE `model_has_roles`  (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`, `model_id`, `model_type`) USING BTREE,
  INDEX `model_has_roles_model_id_model_type_index`(`model_id`, `model_type`) USING BTREE,
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of model_has_roles
-- ----------------------------
INSERT INTO `model_has_roles` VALUES (1, 'App\\Models\\User', 1);
INSERT INTO `model_has_roles` VALUES (2, 'App\\Models\\User', 2);
INSERT INTO `model_has_roles` VALUES (3, 'App\\Models\\User', 3);
INSERT INTO `model_has_roles` VALUES (3, 'App\\Models\\User', 4);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES (1, 'view-dashboard', 'web', '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `permissions` VALUES (2, 'create-post', 'web', '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `permissions` VALUES (3, 'edit-post', 'web', '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `permissions` VALUES (4, 'delete-post', 'web', '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `permissions` VALUES (5, 'manage-authors', 'web', '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `permissions` VALUES (6, 'author-section', 'web', '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `permissions` VALUES (7, 'create-category', 'web', '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `permissions` VALUES (8, 'edit-category', 'web', '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `permissions` VALUES (9, 'delete-category', 'web', '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `permissions` VALUES (10, 'create-company', 'web', '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `permissions` VALUES (11, 'edit-company', 'web', '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `permissions` VALUES (12, 'delete-company', 'web', '2022-01-24 09:18:09', '2022-01-24 09:18:09');

-- ----------------------------
-- Table structure for post_user
-- ----------------------------
DROP TABLE IF EXISTS `post_user`;
CREATE TABLE `post_user`  (
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`post_id`, `user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `job_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_level` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `vacancy_count` smallint(5) UNSIGNED NOT NULL,
  `employment_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deadline` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `education_level` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `skills` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `specifications` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `views` mediumint(8) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES (1, 1, 'Php laravel developer', 'Senior level', 3, 'full time', '25.000.000', 'kathmandu-18,Nepal', '2022-01-30 20:08:04', 'bachelors', '2 years', 'Team player, Active listener', '<p></p>', 4, '2022-01-24 09:18:09', '2022-01-27 06:47:23');
INSERT INTO `posts` VALUES (2, 1, 'Marketing Expert', 'Senior level', 4, 'full time', '7.000.000', 'kathmandu-18,Nepal', '2022-01-30 20:08:09', 'bachelors', '2 years', 'Team player, Active listener', '<p></p>', 1, '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `posts` VALUES (3, 1, 'Professional designer', 'Top level', 4, 'Part time', '2.000.000', 'kathmandu-18,Nepal', '2022-01-30 20:23:41', 'bachelors', '2 years', 'Team player, Active listener', '<p></p>', 1, '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `posts` VALUES (4, 1, 'Dotnet programmer', 'Senior level', 9, 'full time', '9.000.000', 'kathmandu-18,Nepal', '2022-01-30 20:08:17', 'high school', '2 years', 'Team player, Active listener', '<p></p>', 1, '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `posts` VALUES (5, 1, 'Sales Executive', 'Senior level', 2, 'Part time', '2.500.000', 'kathmandu-18,Nepal', '2022-01-30 23:49:45', 'bachelors', '2 years', 'Team player, Active listener', '<p></p>', 1, '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `posts` VALUES (6, 1, 'Maths Teacher', 'Senior level', 5, 'full time', '5.500.000', 'kathmandu-18,Nepal', '2022-01-30 20:08:28', 'master', '2 years', 'Team player, Active listener', '<p></p>', 1, '2022-01-24 09:18:09', '2022-01-24 09:18:09');

-- ----------------------------
-- Table structure for role_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE `role_has_permissions`  (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `role_id`) USING BTREE,
  INDEX `role_has_permissions_role_id_foreign`(`role_id`) USING BTREE,
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_has_permissions
-- ----------------------------
INSERT INTO `role_has_permissions` VALUES (1, 1);
INSERT INTO `role_has_permissions` VALUES (2, 1);
INSERT INTO `role_has_permissions` VALUES (2, 2);
INSERT INTO `role_has_permissions` VALUES (3, 1);
INSERT INTO `role_has_permissions` VALUES (3, 2);
INSERT INTO `role_has_permissions` VALUES (4, 1);
INSERT INTO `role_has_permissions` VALUES (4, 2);
INSERT INTO `role_has_permissions` VALUES (5, 1);
INSERT INTO `role_has_permissions` VALUES (6, 1);
INSERT INTO `role_has_permissions` VALUES (6, 2);
INSERT INTO `role_has_permissions` VALUES (7, 1);
INSERT INTO `role_has_permissions` VALUES (8, 1);
INSERT INTO `role_has_permissions` VALUES (9, 1);
INSERT INTO `role_has_permissions` VALUES (10, 1);
INSERT INTO `role_has_permissions` VALUES (10, 2);
INSERT INTO `role_has_permissions` VALUES (11, 1);
INSERT INTO `role_has_permissions` VALUES (11, 2);
INSERT INTO `role_has_permissions` VALUES (12, 1);
INSERT INTO `role_has_permissions` VALUES (12, 2);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 'admin', 'web', '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `roles` VALUES (2, 'author', 'web', '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `roles` VALUES (3, 'user', 'web', '2022-01-24 09:18:09', '2022-01-24 09:18:09');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `two_factor_recovery_codes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin user', 'admin@admin.com', '2022-01-24 09:18:09', '$2a$12$Axg4vmxuEW1U1GBovuHsZumzYt//HcIiZ/sJxFWwiaj5Cx8IMLCAq', NULL, NULL, 'o9RaijH3VR64R5Z8LEG3lZLubl4v4doymGii4WrKVhWi7DwHpyAcLVFqQs9V', '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `users` VALUES (2, 'author user', 'author@author.com', '2022-01-24 09:18:09', '$2a$12$JIBwxh/hd43Asz19hFGzyeShMh6dU2AKHVJXmj.b9g/QnLiFBVk9e', NULL, NULL, '8nRitV4U9P2U9xVKhUvcPiceY4nH3AFRcaGgkno4XP3JgsSKRJlOauV9U6To', '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `users` VALUES (3, 'simple user', 'user@user.com', '2022-01-24 09:18:09', '$2a$12$don085P1I45mWyC1FOTRSu34YmYgvfmNgn7cJiCK3dP21OD/Gr23C', NULL, NULL, 'muJfCVdb1TrzE98lwAYvkDXGNVKiM0dSFS4EFa5qPnNKxcInZepvLqVQXu9G', '2022-01-24 09:18:09', '2022-01-24 09:18:09');
INSERT INTO `users` VALUES (4, 'Bambang', 'wisnubambang@gmail.com', NULL, '$2a$12$j6J0BXqf9lDmkhooh7Tet.Hxf10N1bI5EeaCs9ZHrBobxuYqk.Q1S', NULL, NULL, NULL, '2022-01-24 09:20:24', '2022-01-24 09:20:24');

SET FOREIGN_KEY_CHECKS = 1;
