<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\CompanyCategory;
use App\Models\Post;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class JobController extends Controller
{
    public function index()
    {
        return view('job.index');
        //return view('jobFinder.pages.home');
    }

    //api route
    public function search(Request $request)
    {
        if ($request->q) {
            $posts = Post::where('job_title', 'LIKE', '%' . $request->q . '%');
        } elseif ($request->category_id) {
            $posts = Post::whereHas('company', function ($query) use ($request) {
                return $query->where('company_category_id', $request->category_id);
            });
        } elseif ($request->job_level) {
            $posts = Post::where('job_level', 'Like', '%' . $request->job_level . '%');
        } elseif ($request->education_level) {
            $posts = Post::where('education_level', 'Like', '%' . $request->education_level . '%');
        } elseif ($request->salary) {
            $posts = Post::where('salary', 'Like', '%' . $request->salary . '%');    
        } elseif ($request->employment_type) {
            $posts = Post::where('employment_type', 'Like', '%' . $request->employment_type . '%');
        } else {
            $posts = Post::take(30);
        }

        $posts = $posts->has('company')->with('company')->paginate(6);

        return $posts->toJson();
    }
    public function getCategories()
    {
        $categories = CompanyCategory::all();
        return $categories->toJson();
    }
    public function getAllOrganization()
    {
        $companies = Company::all();
        return $companies->toJson();
    }
    public function getAllOrganization_Count(Request $request)
    {
        $companies = Post::where('company_id', $request->id)->get()->count();
        return $companies;
    }
    public function getCategories_company(Request $request)
    {
        $categories = CompanyCategory::paginate($request->page);
        return $categories->toJson();
    }
    public function getAllOrganization_company(Request $request)
    {
        $companies = Company::paginate($request->page);
        return $companies->toJson();
    }
    public function getBy_company(Request $request)
    {
        $item_header = Company::where('id', $request->id)->get();
        $item_detail = Post::where('company_id', $request->id)->paginate(4);
        return Response::json(array(
            'data_header' => $item_header,
            'data_detail' => $item_detail,
        ));
        //return $companies->toJson();
    }

    public function getAllByTitle()
    {
        $posts = Post::where('deadline', '>', Carbon::now())->get()->pluck('id', 'job_title');
        return $posts->toJson();
    }

    public function getBy_perJob()
    {
        $posts = DB::table('v_posts')->select('*')->get()->random(4);
        return response()->json($posts);
    }
    
    // public function SearchJobFinder(Request $request)
    // {
    //     if (!empty($request->company_id && $request->job_level && $request->job_location)) {
    //         $posts = DB::table('v_posts')->select('*')->where('company_category_id', 'Like', '%' . $request->company_id . '%')
    //         ->orWhere('job_level', 'Like', '%' . $request->job_level . '%')
    //         ->orWhere('job_location', 'Like', '%' . $request->job_location . '%')
    //         ->get();
    //        return $posts->toJson();
    //     }
    //     else
    //     if
    //         (!empty($request->job_level)) {
    //             $posts = DB::table('v_posts')->select('*')->where('job_level', 'Like', '%' . $request->job_level . '%')->get();
    //            return $posts->toJson();
    //     }elseif 
    //         (!empty($request->company_id)) {
    //             $posts = DB::table('v_posts')->select('*')->where('company_id', 'Like', '%' . $request->company_id . '%')->get();
    //             return $posts->toJson();
    //     }elseif
    //         (!empty($request->job_location)) {
    //             $posts = DB::table('v_posts')->select('*')->where('job_location', 'Like', '%' . $request->job_location . '%')->get();
    //            return $posts->toJson();
    //     }else{
    //         $posts = DB::table('v_posts')->select('*')->take(30)->get(); 
    //     }
    //     $posts = DB::table('v_posts')->select('*')->paginate(5)->get(); 
    //     return $posts->toJson();         
    //   }
    
    // 31 maret 2022 sore
    public function SearchJobFinder(Request $request)
    {
        if (!empty($request->company_id) || ($request->job_level) || ($request->job_location)) {
            $posts = DB::table('v_posts')->select('*')->where('company_category_id', 'Like', '%' . $request->company_id . '%')
            ->Where('job_level', 'Like', '%' . $request->job_level . '%')
            ->Where('job_location', 'Like', '%' . $request->job_location . '%')
            ->paginate(5);
           return response()->json($posts);
        }
        elseif
            (!empty($request->job_level)) {
                $posts = DB::table('v_posts')->select('*')->where('job_level', 'Like', '%' . $request->job_level . '%')->paginate(5);
               return response()->json($posts);
        }elseif 
            (!empty($request->company_id)) {
                $posts = DB::table('v_posts')->select('*')->where('company_id', 'Like', '%' . $request->company_id . '%')->paginate(5);
                return response()->json($posts);
        }elseif
            (!empty($request->job_location)) {
                $posts = DB::table('v_posts')->select('*')->where('job_location', 'Like', '%' . $request->job_location . '%')->paginate(5);
               return response()->json($posts);
        }else{
            $posts = DB::table('v_posts')->select('*')->paginate(5);
        }
        return response()->json($posts);         
      }



   }

