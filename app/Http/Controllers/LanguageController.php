<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LanguageController extends Controller
{
   public function switch($language)
   {
    //    request()->session()->put('locale', $language);
    //    return redirect()->back();
    Session::put('locale', $language);
    return redirect()->back();
   }
}
