<?php

namespace App\Http\Middleware;

use Closure;

class SetLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \App::setLocale($request->language);

        return $next($request);
    }
    //    public function handle(Request $request, Closure $next)
    // {
    //     $locale = Session::get('app_locale');
    //     if (empty($locale)) {
    //         $locale = $this->getVisitorLocale($request->ip());
    //         Session::put('app_locale', $locale);
    //     }

    //     App::setlocale($locale);

    //     return $next($request);
    // }

}
